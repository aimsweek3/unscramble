package com.example.lynn.unscramble;

import android.content.Context;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.example.lynn.unscramble.MainActivity.*;

/**
 * Created by lynn on 6/22/2016.
 */
public class ControlPanel extends LinearLayout {

    public ControlPanel(Context context) {
        super(context);

        play = new Button(context);

        play.setText("Play");

        play.setTextColor(0xFFFFFFFF);

        play.setOnClickListener(listener);

        submit = new Button(context);

        submit.setText("Submit Answer");

        submit.setTextColor(0xFFFFFFFF);

        submit.setOnClickListener(listener);

        message = new TextView(context);

        message.setTextSize(20);

        message.setTextColor(0xFFFFFFFF);

        addView(play);

        addView(submit);

        addView(message);
    }

}
