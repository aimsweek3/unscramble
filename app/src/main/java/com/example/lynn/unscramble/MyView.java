package com.example.lynn.unscramble;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import static com.example.lynn.unscramble.MainActivity.*;


/**
 * Created by lynn on 6/20/2016.
 */
public class MyView extends TableLayout {

    public String[] getWord() {
        String[] words = new String[]{"Abandon",
                "abundant",
                "access",
                "accommodate",
                "accumulate",
                "adapt",
                "adhere",
                "agony",
                "allegiance",
                "ambition",
                "ample",
                "anguish",
                "anticipate",
                "anxious",
                "apparel",
                "appeal",
                "apprehensive",
                "arid",
                "arrogant",
                "awe",
                "Barren",
                "beacon",
                "beneficial",
                "blunder",
                "boisterous",
                "boycott",
                "burden",
                "Campaign",
                "capacity",
                "capital",
                "chronological",
                "civic",
                "clarity",
                "collaborate",
                "collide",
                "commend",
                "commentary",
                "compact",
                "composure",
                "concise",
                "consent",
                "consequence",
                "conserve",
                "conspicuous",
                "constant",
                "contaminate",
                "context",
                "continuous",
                "controversy",
                "convenient",
                "cope",
                "cordial",
                "cultivate",
                "cumulative",
                "Declare",
                "deluge",
                "dense",
                "deplete",
                "deposit",
                "designate",
                "desperate",
                "deteriorate",
                "dialogue",
                "diligent",
                "diminish",
                "discretion",
                "dissent",
                "dissolve",
                "distinct",
                "diversity",
                "domestic",
                "dominate",
                "drastic",
                "duration",
                "dwell",
                "Eclipse",
                "economy",
                "eerie",
                "effect",
                "efficient",
                "elaborate",
                "eligible",
                "elude",
                "encounter",
                "equivalent",
                "erupt",
                "esteem",
                "evolve",
                "exaggerate",
                "excel",
                "exclude",
                "expanse",
                "exploit",
                "extinct",
                "extract",
                "Factor",
                "former",
                "formulates",
                "fuse",
                "futile",
                "Generate",
                "genre",
                "Habitat",
                "hazardous",
                "hoax",
                "hostile",
                "Idiom",
                "ignite",
                "immense",
                "improvises",
                "inept",
                "inevitable",
                "influence",
                "ingenious",
                "innovation",
                "intimidate",
                "Jovial",
                "Knack",
                "Leeway",
                "legislation",
                "leisure",
                "liberate",
                "likeness",
                "linger",
                "literal",
                "loathe",
                "lure",
                "Majority",
                "makeshift",
                "manipulate",
                "marvel",
                "massive",
                "maximum",
                "meager",
                "mere",
                "migration",
                "mimic",
                "minute",
                "monotonous",
                "Negotiate",
                "Objective",
                "obstacle",
                "omniscient",
                "onset",
                "optimist",
                "originate",
                "Painstaking",
                "paraphrase",
                "parody",
                "persecute",
                "plummet",
                "possess",
                "poverty",
                "precise",
                "predicament",
                "predict",
                "prejudice",
                "preliminary",
                "primitive",
                "priority",
                "prominent",
                "propel",
                "prosecute",
                "prosper",
                "provoke",
                "pursue",
                "Quest",
                "Recount",
                "refuge",
                "reinforce",
                "reluctant",
                "remorse",
                "remote",
                "resolute",
                "restrain",
                "retaliate",
                "retrieve",
                "rigorous",
                "rural",
                "Salvage",
                "sanctuary",
                "siege",
                "significant",
                "solar",
                "soothe",
                "stationary",
                "stifle",
                "strive",
                "subordinate",
                "subsequent",
                "superior",
                "supplement",
                "swarm",
                "Tangible",
                "terminate",
                "terrain",
                "trait",
                "transform",
                "transport",
                "treacherous",
                "Unanimous",
                "unique",
                "unruly",
                "urban",
                "Vacate",
                "verdict",
                "verge",
                "vibrant",
                "vital",
                "vow"};

        for (int counter=0;counter<words.length;counter++)
            words[counter] = words[counter].toUpperCase();

        return(words);
    }

        public TextView createTextView(Context context,
                                       String text) {
                TextView textView = new TextView(context);

                textView.setText(text);

                textView.setTextColor(0xFFFFFFFF);

                textView.setTextSize(40);

                return(textView);
        }

    public MyView(Context context) {
        super(context);

            drawables = new Drawable[10];

            drawables[0] = getResources().getDrawable(R.drawable.zero);
            drawables[1] = getResources().getDrawable(R.drawable.one);
            drawables[2] = getResources().getDrawable(R.drawable.two);
            drawables[3] = getResources().getDrawable(R.drawable.three);
            drawables[4] = getResources().getDrawable(R.drawable.four);
            drawables[5] = getResources().getDrawable(R.drawable.five);
            drawables[6] = getResources().getDrawable(R.drawable.six);
            drawables[7] = getResources().getDrawable(R.drawable.seven);
            drawables[8] = getResources().getDrawable(R.drawable.eight);
            drawables[9] = getResources().getDrawable(R.drawable.nine);

            minus = getResources().getDrawable(R.drawable.minus);

            setBackground(getResources().getDrawable(R.drawable.background));

            TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);

            setLayoutParams(layoutParams);

            TableRow row1 = new TableRow(context);

            TableRow row2 = new TableRow(context);

            score = -200;

            row1.addView(controlPanel = new ControlPanel(context));

            row1.addView(createTextView(context,"Score: "));

            row1.addView(scorePanel = new ScorePanel(context));

            row2.addView(gamePanel = new GamePanel(context));

            addView(row1);
            addView(row2);
    }

}
