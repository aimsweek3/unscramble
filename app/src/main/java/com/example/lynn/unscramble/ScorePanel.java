package com.example.lynn.unscramble;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

import static com.example.lynn.unscramble.MainActivity.*;

/**
 * Created by lynn on 6/22/2016.
 */
public class ScorePanel extends LinearLayout {
    private Context context;

    public ImageView createImageView(int number) {
        ImageView view = new ImageView(context);

        if (number < 0)
            view.setImageDrawable(minus);
        else
            view.setImageDrawable(drawables[number]);

        return(view);
    }

    public ScorePanel(Context context) {
        super(context);

        this.context = context;

        update();
    }

    public void update() {
        String theScore = String.valueOf(score);

        removeAllViews();

        views = new ImageView[theScore.length()];

        for (int counter=0;counter<theScore.length();counter++) {
            if (theScore.charAt(counter) == '-')
                addView(createImageView(-1));
            else
                addView(createImageView(Integer.parseInt(theScore.substring(counter,counter+1))));
        }
    }

}
