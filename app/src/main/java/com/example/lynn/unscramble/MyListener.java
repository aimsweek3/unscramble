package com.example.lynn.unscramble;

import android.view.View;
import android.widget.Button;

import static com.example.lynn.unscramble.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class MyListener implements View.OnClickListener {
    private Button first;

    public String scramble(String input) {
        char[] characters = input.toCharArray();

        for (int counter=0;counter<100;counter++) {
            int position1 = (int)(characters.length*Math.random());
            int position2 = (int)(characters.length*Math.random());

            char temp = characters[position1];
            characters[position1] = characters[position2];
            characters[position2] = temp;
        }

        return(new String(characters));
    }

    public void showAnswer() {
        if (buttons != null) {
            for (int counter = 0; counter < buttons.length; counter++)
                buttons[counter].setText(word.substring(counter, counter + 1));
        }
    }

    @Override
    public void onClick(View v) {
       if(v instanceof Button) {
           Button source = (Button)v;

           if (source == play) {
               String[] words = myView.getWord();

               word = words[(int)(words.length*Math.random())];

               if (buttons != null) {
                   for (int counter=0;counter<buttons.length;counter++)
                       gamePanel.removeView(buttons[counter]);
               }

               scrambledWord = scramble(word);

               buttons = new Button[scrambledWord.length()];

               for(int counter=0;counter<buttons.length;counter++) {
                   buttons[counter] = new Button(play.getContext());

                   buttons[counter].setTextSize(40);

                   buttons[counter].setText(scrambledWord.substring(counter,counter+1));

                   buttons[counter].setTextColor(0xFFFFFFFF);

                   buttons[counter].setOnClickListener(listener);

                   gamePanel.addView(buttons[counter]);
               }
           } else if (source == submit) {
              String myWord = "";

               for (int counter=0;counter<buttons.length;counter++)
                   myWord += buttons[counter].getText().toString();

               if (myWord.equals(word)) {
                   message.setText("Right");

                   score += 10;
               } else {
                   message.setText("The right answer was " + word);

                   for (int counter=0;counter<buttons.length;counter++)
                       buttons[counter].setText("");

                   score -= 10;
               }

               scorePanel.update();

           } else {
               if (first == null)
                   first = source;
               else {
                   Button second = source;

                   String temp = first.getText().toString();
                   first.setText(second.getText());
                   second.setText(temp);

                   first = null;
               }

           }

       }


    }

}
